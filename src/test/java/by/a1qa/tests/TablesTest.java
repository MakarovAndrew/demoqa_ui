package by.a1qa.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.base.BaseTest;
import by.a1qa.forms.LeftPanelForm;
import by.a1qa.forms.MainPage;
import by.a1qa.forms.WebTablesPage;
import by.a1qa.models.User;
import by.a1qa.utils.DataProviderClass;
import static by.a1qa.utils.SimpleLogger.logger;

public class TablesTest extends BaseTest{

    private MainPage mainPage = new MainPage();
    private LeftPanelForm leftPanelForm = new LeftPanelForm();
    private WebTablesPage webTablesPage = new WebTablesPage();

    @Test (description = "#3. Tables", dataProvider = "fromFile", dataProviderClass = DataProviderClass.class)
    public void checkTables(User user) {

        logger.info("Test #3. Tables for User: " + user.getFirstName() + " " + user.getLastName() + " is started...");
        logger.info("Starting Step 1...");
        Assert.assertTrue(mainPage.isFormOpened(), "Main page was not opened!");

        logger.info("Starting Step 2...");
        mainPage.clickElementsButton();
        leftPanelForm.clickWebTablesButton();
        Assert.assertTrue(webTablesPage.isFormOpened(),"No Web Tables form appeared!");

        logger.info("Starting Step 3...");
        webTablesPage.clickAddNewRecordButton();
        Assert.assertTrue(webTablesPage.registrationFormIsOpened(), "Registration form was not opened!");

        logger.info("Starting Step 4...");
        webTablesPage.fillRegistrationForm(user);
        webTablesPage.addUserToUserTable();
        Assert.assertTrue(webTablesPage.registrationFormIsInvisible(), "Registration form still opened!");
        Assert.assertTrue(webTablesPage.userTableContainsUser(user), "The added user is not contained in the table!");

        logger.info("Starting Step 5...");
        webTablesPage.removeAddedUser(user);
        Assert.assertTrue(webTablesPage.numberOfUsersHasChanged(), "Rows count did not change!");
        Assert.assertFalse(webTablesPage.userTableContainsUser(user), "User still present in the user table!");
    }
}