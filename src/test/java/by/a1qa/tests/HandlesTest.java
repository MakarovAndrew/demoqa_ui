package by.a1qa.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.base.BaseTest;
import by.a1qa.forms.BrowserWindowsPage;
import by.a1qa.forms.LeftPanelForm;
import by.a1qa.forms.LinksPage;
import by.a1qa.forms.MainPage;
import by.a1qa.forms.SampleTabPage;
import by.a1qa.utils.WindowHandle;
import static by.a1qa.utils.SimpleLogger.logger;
import static by.a1qa.driver.BrowserFactory.*;

public class HandlesTest extends BaseTest {

    private String baseHandle = null;
    
    private MainPage mainPage = new MainPage();
    private BrowserWindowsPage browserWindowsPage = new BrowserWindowsPage();
    private SampleTabPage tabPage = new SampleTabPage();
    private LinksPage linksPage = new LinksPage();
    private LeftPanelForm leftPanelForm = new LeftPanelForm();

    @Test (description = "#4. Handles")
    public void checkHandles() {
        
        baseHandle = getDriver().getWindowHandle();
        int numberOfExpectedTab = 2;

        logger.info("Test #4. Handles is started...");
        logger.info("Starting Step 1...");
        Assert.assertTrue(mainPage.isFormOpened(), "Main page was not opened!");

        logger.info("Starting Step 2...");
        mainPage.clickAlertsFrameWindowsButton();
        leftPanelForm.clickBrowserWindowsButton();
        Assert.assertTrue(browserWindowsPage.isFormOpened(),"No browser windows form appeared!");

        logger.info("Starting Step 3...");
        browserWindowsPage.clickNewTabButton();
        WindowHandle.focusToNewTab(baseHandle, numberOfExpectedTab);
        Assert.assertTrue(tabPage.isFormOpened(), "New tab was not opened!");

        logger.info("Starting Step 4...");
        closeDriver();
        WindowHandle.switchToWindow(baseHandle);
        Assert.assertTrue(browserWindowsPage.isFormOpened(),"No browser windows form appeared!");

        logger.info("Starting Step 5...");
        leftPanelForm.clickElementsButton();
        leftPanelForm.clickLinksButton();
        Assert.assertTrue(linksPage.isFormOpened(), "No link form appeared!");
        
        logger.info("Starting Step 6...");
        linksPage.clickHomeLink();
        WindowHandle.focusToNewTab(baseHandle, numberOfExpectedTab);
        Assert.assertTrue(mainPage.isFormOpened(), "Main page was not opened!");

        logger.info("Starting Step 7...");
        getDriver().switchTo().window(baseHandle);
        Assert.assertTrue(linksPage.isFormOpened(), "No link form appeared!");
    }
}