package by.a1qa.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.base.BaseTest;
import by.a1qa.forms.AlertsFramesAndWindowsPage;
import by.a1qa.forms.LeftPanelForm;
import by.a1qa.forms.MainPage;
import by.a1qa.utils.Randomizer;
import static by.a1qa.utils.WindowHandle.*;
import static by.a1qa.utils.Waiters.*;
import static by.a1qa.utils.SimpleLogger.logger;

public class AlertsTest extends BaseTest {

    private String name = Randomizer.returnRandomString(20);

    private MainPage mainPage = new MainPage();
    private AlertsFramesAndWindowsPage alertsFramesAndWindowsPage = new AlertsFramesAndWindowsPage();
    private LeftPanelForm leftPanelForm = new LeftPanelForm();

    @Test (description = "#1. Alerts")
    public void checkAlerts() {

        logger.info("Test #1. ALerts is started...");
        logger.info("Starting Step 1...");
        Assert.assertTrue(mainPage.isFormOpened(), "Main page was not opened!");

        logger.info("Starting Step 2...");
        mainPage.clickAlertsFrameWindowsButton();
        leftPanelForm.clickAlertsButton();
        Assert.assertTrue(alertsFramesAndWindowsPage.isFormOpened(),"No alerts form appeared!");

        logger.info("Starting Step 3...");
        alertsFramesAndWindowsPage.clickClickToSeeButton();
        Assert.assertEquals(switchToAlert().getText(), "You clicked a button", "Requred alert was not found!");

        logger.info("Starting Step 4...");
        waitUntilAlert(SHORT_TIMEOUT);
        switchToAlert().accept();
        Assert.assertFalse(isAlertPresent(), "Alert was not closed!");

        logger.info("Starting Step 5...");
        alertsFramesAndWindowsPage.clickClickConfirmButton();;
        Assert.assertEquals(switchToAlert().getText(), "Do you confirm action?", "Requred alert was not found!");

        logger.info("Starting Step 6...");
        switchToAlert().accept();
        Assert.assertFalse(isAlertPresent(), "Alert was not closed!");
        Assert.assertEquals(alertsFramesAndWindowsPage.getConfirmResultText(), "You selected Ok", "Text 'You selected Ok' was not displayed!");

        logger.info("Starting Step 7...");
        alertsFramesAndWindowsPage.clickClickPromptButton();
        Assert.assertEquals(switchToAlert().getText(), "Please enter your name", "Requred alert was not found!");

        logger.info("Starting Step 8...");
        switchToAlert().sendKeys(name);
        switchToAlert().accept();
        Assert.assertFalse(isAlertPresent(), "Alert was not closed!");
        Assert.assertEquals(alertsFramesAndWindowsPage.getEnteredNameText(), "You entered " + name, "Text 'You selected " + name + "' was not displayed!");
    }
}