package by.a1qa.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import by.a1qa.base.BaseTest;
import by.a1qa.forms.FramesPage;
import by.a1qa.forms.LeftPanelForm;
import by.a1qa.forms.MainPage;
import by.a1qa.forms.NestedFramesPage;
import static by.a1qa.utils.SimpleLogger.logger;

public class IframeTest extends BaseTest{

    private MainPage mainPage = new MainPage();
    private LeftPanelForm leftPanelForm = new LeftPanelForm();
    private NestedFramesPage nestedFramesPage = new NestedFramesPage();
    private FramesPage framesPage = new FramesPage();

    @Test (description = "#2. Iframe")
    public void checkIframe() {

        logger.info("Test #2. Iframe is started...");
        logger.info("Starting Step 1...");
        Assert.assertTrue(mainPage.isFormOpened(), "Main page was not opened!");

        logger.info("Starting Step 2...");
        mainPage.clickAlertsFrameWindowsButton();
        leftPanelForm.clickNestedFramesButton();
        Assert.assertTrue(nestedFramesPage.isFormOpened(),"No Nested frame form appeared!");
        Assert.assertEquals(nestedFramesPage.switchToParentFrameAndGetText(), "Parent frame", "Text 'Parent frame' was not displayed!");
        Assert.assertEquals(nestedFramesPage.switchToChildFrameAndGetText(), "Child Iframe", "Text 'Child Iframe' was not displayed!");
        
        logger.info("Starting Step 3...");
        leftPanelForm.clickFramesButton();
        Assert.assertTrue(framesPage.isFormOpened(),"No Frames form appeared!");
        Assert.assertEquals(framesPage.switchToUpperFrameAndGetText(), framesPage.switchToLowerFrameAndGetText(), "Messages are not equal!");
    }
}