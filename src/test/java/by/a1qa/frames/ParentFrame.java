package by.a1qa.frames;

import org.openqa.selenium.By;
import by.a1qa.base.BaseFrame;
import by.a1qa.elements.Label;

public class ParentFrame extends BaseFrame{

    private static final By anchor = By.id("frame1");

    private final Label parentFrameMessage = new Label(By.tagName("body"), "Parent frame message");

    public ParentFrame() {
        super(anchor, "Parent nested frame");
    }

    public String getParentFrameMessage() {
        return parentFrameMessage.getText();
    }
}