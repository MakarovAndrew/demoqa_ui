package by.a1qa.frames;

import org.openqa.selenium.By;
import by.a1qa.base.BaseFrame;
import by.a1qa.forms.SampleTabPage;

public class LowerFrame extends BaseFrame{

    private static final By anchor = By.xpath("//iframe[@src='/sample' and @id='frame2']");

    private final SampleTabPage sampleTabPage = new SampleTabPage();

    public LowerFrame() {
        super(anchor, "Lower frame");
    }

    public String getLowerFrameMessage() {
        return sampleTabPage.getAnchorText();
    }
}