package by.a1qa.frames;

import org.openqa.selenium.By;
import by.a1qa.base.BaseFrame;
import by.a1qa.forms.SampleTabPage;

public class UpperFrame extends BaseFrame{

    private static final By anchor = By.xpath("//iframe[@src='/sample' and @id='frame1']");

    private final SampleTabPage sampleTabPage = new SampleTabPage();

    public UpperFrame() {
        super(anchor, "Upper frame");
    }

    public String getUpperFrameMessage() {
        return sampleTabPage.getAnchorText();
    }
}