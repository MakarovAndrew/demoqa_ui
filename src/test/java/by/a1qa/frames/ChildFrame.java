package by.a1qa.frames;

import org.openqa.selenium.By;
import by.a1qa.base.BaseFrame;
import by.a1qa.elements.Label;

public class ChildFrame extends BaseFrame{

    private static final By anchor = By.xpath("//iframe[@srcdoc='<p>Child Iframe</p>']");

    private final Label childFrameMessage = new Label(By.tagName("p"), "Child frame message");

    public ChildFrame() {
        super(anchor, "Child nested frame");
    }

    public String getChildFrameMessage() {
        return childFrameMessage.getText();
    }
}