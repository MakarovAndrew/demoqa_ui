package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.frames.ChildFrame;
import by.a1qa.frames.ParentFrame;
import static by.a1qa.utils.SimpleLogger.logger;

public class NestedFramesPage extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//text()[.='Nested Frames']/parent::div"), "Nested frames page anchor");

    private final ParentFrame parentFrame = new ParentFrame();
    private final ChildFrame childFrame = new ChildFrame();

    public NestedFramesPage() {
        super(anchor, "Nested frames page");
    }

    public String switchToParentFrameAndGetText(){
        String parentFrameMessage;
        logger.info("Switching to Parrent frame and getting text...");
        parentFrame.switchToFrame();
        parentFrameMessage = parentFrame.getParentFrameMessage();
        parentFrame.switchToDefaultContent();
        return parentFrameMessage;
    }

    public String switchToChildFrameAndGetText(){
        String childFrameMessage;
        logger.info("Switching to Child Iframe over Parent iframe and getting text...");
        parentFrame.switchToFrame();
        childFrame.switchToFrame();
        childFrameMessage = childFrame.getChildFrameMessage();
        childFrame.switchToDefaultContent();
        return childFrameMessage;
    }
}