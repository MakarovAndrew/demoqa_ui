package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseElement;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.models.User;
import by.a1qa.elements.Button;

public class WebTablesPage extends BaseForm{

    private static final BaseElement anchor = new Label(By.xpath("//div[@class='web-tables-wrapper']"), "Web Tables page anchor");

    private final Button addNewRecordButton = new Button(By.xpath("//button[@id='addNewRecordButton']"), "Add new record button");
    private final UserTableForm userTableForm = new UserTableForm();
    private final RegistrationForm registrationForm = new RegistrationForm();

    public WebTablesPage() {
        super(anchor, "Web Tables page");
    }

    public void clickAddNewRecordButton() {
        addNewRecordButton.clickElement();
    }

    public boolean registrationFormIsOpened() {
        return registrationForm.isFormOpened();
    }

    public void addUserToUserTable() {
        registrationForm.clickSubmitButton();
    }

    public boolean registrationFormIsInvisible() {
        return registrationForm.isInvisible();
    }

    public void fillRegistrationForm(User user) {
        registrationForm.fillForm(user);
    }

    public boolean userTableContainsUser(User user) {
        return userTableForm.parseUserTableToUserClass().contains(user);
    }

    public void removeAddedUser(User user) {
        userTableForm.initAndClickDeleteButton(user);
    }

    public boolean numberOfUsersHasChanged() {
        return userTableForm.getUsersBeforeDeleteButtonClick() != userTableForm.countOfNotEmptyRows();
    }
}