package by.a1qa.forms;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;
import by.a1qa.models.User;
import static by.a1qa.utils.SimpleLogger.logger;

public class UserTableForm extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//div[@class='rt-table']"), "User table anchor");

    private List<WebElement> userList;
    private List<User> userObjectList;
    private int usersBeforeDeleteButtonClick;

    public UserTableForm() {
        super(anchor, "User table");
    }

    public int getUsersBeforeDeleteButtonClick() {
        return usersBeforeDeleteButtonClick;
    }

    public void initAndClickDeleteButton(User user) {
        Button deleteButton = new Button(By.xpath("//span[contains(@id,'delete-record-" + (userObjectList.lastIndexOf(user)+1) + "')]"),"Delete Button for added user");
        deleteButton.clickElement();
    }

    By notEmptyRowsLocator = By.xpath("//div[@role='rowgroup']/div[not(contains(@class,'-padRow'))]");

    By userNameCell = By.xpath(".//div[@class='rt-td'][1]");
    By userLastNameCell = By.xpath(".//div[@class='rt-td'][2]");
    By userEmailCell = By.xpath(".//div[@class='rt-td'][4]");
    By userAgeCell = By.xpath(".//div[@class='rt-td'][3]");
    By userSalaryCell = By.xpath(".//div[@class='rt-td'][5]");
    By userDepartamentCell = By.xpath(".//div[@class='rt-td'][6]");

    private final String cellLocator = ".//div[@class='rt-td'][%d]";
    
    private final int INDEX_NAME = 1;
    private final int INDEX_LAST_NAME = 2;
    private final int INDEX_AGE = 3;
    private final int INDEX_EMAIL = 4;
    private final int INDEX_SALARY = 5;
    private final int INDEX_DEPAETAMENT = 6;

    private By formatedLocator(int index) {
        return By.xpath(String.format(cellLocator, index));
    }

    private String getCellValue(WebElement element, int index) {
        return element.findElement(formatedLocator(index)).getText();
    }

    public List<User> parseUserTableToUserClass() {

        logger.info("Getting list of Users from table...");
        userList = this.getElements(notEmptyRowsLocator);
        usersBeforeDeleteButtonClick = userList.size();
        userObjectList = new ArrayList<>();

            for (WebElement user : userList) {
                userObjectList.add(
                    new User(
                        getCellValue(user, INDEX_NAME),
                        getCellValue(user, INDEX_LAST_NAME),
                        getCellValue(user, INDEX_EMAIL),
                        getCellValue(user, INDEX_AGE),
                        getCellValue(user, INDEX_SALARY),
                        getCellValue(user, INDEX_DEPAETAMENT)
                    )
                );
            }
       
        return userObjectList;
    }

    public int countOfNotEmptyRows () {
       return this.getElements(By.xpath("//div[@role='rowgroup']/div[not(contains(@class,'-padRow'))]")).size();
    }
}