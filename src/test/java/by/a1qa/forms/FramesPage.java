package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.frames.LowerFrame;
import by.a1qa.frames.UpperFrame;

public class FramesPage extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//text()[.='Frames']/parent::div"), "Frames page anchor");

    private final UpperFrame upperFrame = new UpperFrame();
    private final LowerFrame lowerFrame = new LowerFrame();

    public FramesPage() {
        super(anchor, "Frames page");
    }

    public String switchToUpperFrameAndGetText() {
        String upperFrameMessage;
        upperFrame.switchToFrame();
        upperFrameMessage = upperFrame.getUpperFrameMessage();
        upperFrame.switchToDefaultContent();
        return upperFrameMessage;
    }

    public String switchToLowerFrameAndGetText() {
        String lowerFrameMessage;
        lowerFrame.switchToFrame();
        lowerFrameMessage = lowerFrame.getLowerFrameMessage();
        lowerFrame.switchToDefaultContent();
        return lowerFrameMessage;
    }
}