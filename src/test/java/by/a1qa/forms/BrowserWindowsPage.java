package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;

public class BrowserWindowsPage extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//div[@id='browserWindows']"), "Browser windows page anchor");

    public final Button newTabButton = new Button(By.xpath("//button[@id='tabButton']"), "New tab button");

    public BrowserWindowsPage() {
        super(anchor, "Browser windows page");
    }

    public void clickNewTabButton() {
        newTabButton.clickElement();
    }
}