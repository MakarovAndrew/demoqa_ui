package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;
import by.a1qa.elements.InputBox;
import by.a1qa.models.User;
import static by.a1qa.utils.SimpleLogger.logger;

public class RegistrationForm extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//form[@id='userForm']"), "Registration form anchor");
    
    private final Button submitButton = new Button(By.xpath("//button[@id='submit']"),"Submit button");
    private final InputBox firstNameInputBox = new InputBox(By.xpath("//input[@id='firstName']"), "First name input box");
    private final InputBox lastNameInputBox = new InputBox(By.xpath("//input[@id='lastName']"), "Last name input box");
    private final InputBox userEmailInputBox = new InputBox(By.xpath("//input[@id='userEmail']"), "User email input box");
    private final InputBox ageInputBox = new InputBox(By.xpath("//input[@id='age']"), "Age input box");
    private final InputBox salaryInputBox = new InputBox(By.xpath("//input[@id='salary']"), "Salary input box");
    private final InputBox departmentInputBox = new InputBox(By.xpath("//input[@id='department']"), "Department input box");

    public RegistrationForm() {
        super(anchor, "Registration form");
    }

    public void clickSubmitButton() {
        submitButton.clickElement();
    }

    public void fillFirstNameField(String value) {
        firstNameInputBox.enterValue(value);
    }

    public void fillLastNameField(String value) {
        lastNameInputBox.enterValue(value);
    }

    public void fillUserEmailField(String value) {
        userEmailInputBox.enterValue(value);
    }

    public void fillAgeField(String value) {
        ageInputBox.enterValue(value);
    }

    public void fillSalaryField(String value) {
        salaryInputBox.enterValue(value);
    }

    public void fillDepartmentField(String value) {
        departmentInputBox.enterValue(value);
    }

    public void fillForm (User user) {
        logger.info("Filling registration form by data from User: " + user.getFirstName() + " " + user.getLastName());
        fillFirstNameField(user.getFirstName());
        fillLastNameField(user.getLastName());
        fillUserEmailField(user.getEmail());
        fillAgeField(user.getAge());
        fillSalaryField(user.getSalary());
        fillDepartmentField(user.getDepartament());
    }
}