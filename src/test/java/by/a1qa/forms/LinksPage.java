package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;

public class LinksPage extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//div[@id='linkWrapper']"), "Links page anchor");

    public final Label homeLink = new Label(By.xpath("//a[@id='simpleLink']"), "Home link");

    public LinksPage() {
        super(anchor, "Links page");
    }

    public void clickHomeLink() {
        homeLink.clickElement();
    }
}