package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;

public class LeftPanelForm extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//div[@class='left-pannel']"), "Left panel anchor");

    private final Button browserWindowsButton = new Button(By.xpath("//li[@id='item-0']/span[contains(text(),'Browser Windows')]"), "Browser Windows button");
    private final Button elementsButton = new Button(By.xpath("//div[./div/text()[contains(.,'Elements')]]"), "Elements Left menu button");
    private final Button linksButton = new Button(By.xpath("//li[@id='item-5']/span[contains(text(),'Links')]"), "Links button");
    private final Button alertsButton = new Button(By.xpath("//li[@id='item-1']/span[contains(text(),'Alerts')]"), "Alerts button");
    private final Button clickConfirmButton = new Button(By.xpath("//button[@id='confirmButton']"), "Confirm box button");
    private final Button clickPromptButton = new Button(By.xpath("//button[@id='promtButton']"), "Prompt box button");
    private final Button nestedFramesButton = new Button(By.xpath("//li[@id='item-3']/span[contains(text(),'Nested')]"), "Nested frames button");
    private final Button framesButton = new Button(By.xpath("//li[@id='item-2']/span[contains(text(),'Frames')]"), "Frames button");
    private final Button webTablesButton = new Button(By.xpath("//li[@id='item-3']/span[contains(text(),'Web Tables')]"), "Web tables button");

    public LeftPanelForm() {
        super(anchor, "Left panel");
    }

    public void clickWebTablesButton() {
        webTablesButton.clickElement();
    }

    public void clickFramesButton() {
        framesButton.clickElement();
    }

    public void clickNestedFramesButton() {
        nestedFramesButton.clickElement();
    }

    public void clickClickConfirmButton() {
        clickConfirmButton.clickElement();
    }

    public void clickClickPromptButton() {
        clickPromptButton.clickElement();
    }

    public void clickBrowserWindowsButton() {
        browserWindowsButton.clickElement();
    }

    public void clickElementsButton() {
        elementsButton.clickElement();
    }

    public void clickLinksButton() {
        linksButton.clickElement();
    }

    public void clickAlertsButton() {
        alertsButton.clickElement();
    }
}