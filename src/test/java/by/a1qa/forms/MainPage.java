package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;

public class MainPage extends BaseForm{

    private static Label anchor = new Label(By.xpath("//div[@class='home-banner']"), "Main page anchor"); 

    public final Button alertsFrameWindowsButton = new Button(By.xpath("//div[@class='category-cards']/div[3]"), "Alerts, frames and windows button");
    public final Button elementsButton = new Button(By.xpath("//div[@class='category-cards']/div[1]"), "Elements button");

    public MainPage() {
        super(anchor, "Main page");
    }

    public void clickElementsButton() {
        elementsButton.clickElement();
    }

    public void clickAlertsFrameWindowsButton() {
        alertsFrameWindowsButton.clickElement();
    }
}