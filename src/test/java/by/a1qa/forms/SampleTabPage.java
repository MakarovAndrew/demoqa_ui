package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;

public class SampleTabPage extends BaseForm{

    private static Label anchor = new Label(By.xpath("//h1[@id='sampleHeading']"), "Sample tab page anchor");

    public SampleTabPage() {
        super(anchor, "Sample tab page");
    }

    public String getAnchorText() {
        return anchor.getText();
    }
}