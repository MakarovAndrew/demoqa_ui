package by.a1qa.forms;

import org.openqa.selenium.By;
import by.a1qa.base.BaseForm;
import by.a1qa.elements.Label;
import by.a1qa.elements.Button;

public class AlertsFramesAndWindowsPage extends BaseForm{

    private static final Label anchor = new Label(By.xpath("//div[@id='javascriptAlertsWrapper']"), "Alerts, Frames & Windows anchor");

    private final Button clickToSeeButton = new Button(By.xpath("//button[@id='alertButton']"), "Click to see button");
    private final Label confirmResultText = new Label(By.xpath("//span[@id='confirmResult']"), "Text 'You selected Ok'");
    private final Button clickPromptButton = new Button(By.xpath("//button[@id='promtButton']"), "Prompt box button");
    private final Button clickConfirmButton = new Button(By.xpath("//button[@id='confirmButton']"), "Confirm box button");
    private final Label enteredName = new Label(By.xpath("//span[@id='promptResult']"), "Text 'You entered name'");

    public AlertsFramesAndWindowsPage() {
        super(anchor, "Alerts, Frames & Windows page");
    }

    public String getEnteredNameText() {
        return enteredName.getText();
    }

    public void clickClickConfirmButton() {
        clickConfirmButton.clickElement();
    }
    
    public void clickClickPromptButton() {
        clickPromptButton.clickElement();
    }

    public void clickClickToSeeButton() {
        clickToSeeButton.clickElement();
    }

    public String getConfirmResultText() {
        return confirmResultText.getText();
    }
    







}