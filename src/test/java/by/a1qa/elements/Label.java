package by.a1qa.elements;

import org.openqa.selenium.By;
import by.a1qa.base.BaseElement;

public class Label extends BaseElement {

    public Label(By locator, String name) {
        super(locator, name);
    }
}