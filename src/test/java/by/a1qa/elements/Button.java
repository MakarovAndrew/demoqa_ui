package by.a1qa.elements;

import org.openqa.selenium.By;
import by.a1qa.base.BaseElement;

public class Button extends BaseElement {
    
    public Button (By locator, String name) {
        super(locator, name);
    }
}