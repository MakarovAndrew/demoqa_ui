package by.a1qa.elements;

import org.openqa.selenium.By;
import by.a1qa.base.BaseElement;
import static by.a1qa.utils.SimpleLogger.logger;

public class InputBox extends BaseElement{

    public InputBox(By locator, String name) {
        super(locator, name);
    }

    public void enterValue(String value) {
        logger.info("Entering value: " + value + " to " + getElementName());
        getElement().sendKeys(value);
    }
}