package by.a1qa.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static by.a1qa.utils.Waiters.*;
import static by.a1qa.utils.SimpleLogger.logger;

public abstract class BaseElement {
    
    private By locator;
    private String name;

    public BaseElement (By locator, String name) {
        this.locator = locator;
        this.name = name;
    }

    public String getElementName() {
        return this.name;
    }

    public By getElementLocator() {
        return this.locator;
    }

    public String getText() {
        logger.info("Getting text from element: " + this.name);
        return getElement().getText();
    }

    public WebElement getElement() {    
        return waitUntilVisibilityOfElementLocated((this.locator), LONG_TIMEOUT);
    }

    public void clickElement() {
        logger.info("Click element: " + this.name);
        waitUntilToBeClickable(getElement(), SHORT_TIMEOUT).click();
    }

    public String getAttribute(String attribute) {
        logger.info("Getting attribute " + attribute + " from element: " + this.name);
        return getElement().getAttribute(attribute);
    }

    public boolean isDisplayed() {
        logger.info("Element " + this.name + " is displayed");
        return  waitUntilIsPresent(getElement(), SHORT_TIMEOUT).isDisplayed();
    }

    public String toString() {
        return "\nElement name: " + name + "\nElement locator: " + locator;
    }
}