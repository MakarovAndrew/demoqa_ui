package by.a1qa.base;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static by.a1qa.driver.BrowserFactory.*;
import static by.a1qa.utils.SimpleLogger.logger;
import static by.a1qa.utils.Waiters.*;

public abstract class BaseForm {

    private final BaseElement element;
    private final String name;
    
    public BaseForm(BaseElement element, String name) {
        this.element = element;
        this.name = name;
    }

    public boolean isFormOpened() {
        logger.info("Page " + name + " is opened");
        return this.element.isDisplayed();
    }

    public boolean isStale() {
        logger.info("Checking element " + this.name + " for staleness...");
        return waitUntilStaleness(getDriver().findElement(this.element.getElementLocator()), SHORT_TIMEOUT);
    }

    public boolean isInvisible() {
        logger.info("Checking element " + this.name + " for invisibility...");
        return waitUntilInvisiblityOfElementLocated(this.element, SHORT_TIMEOUT);
    }

    public List<WebElement> getElements(By locator) {
        return waitUntilVisibilityOfAllElementsLocated(locator, LONG_TIMEOUT);
    }

    public String toString() {
        return "\nPage name: " + name + "\nPage anchor element: " + element.getElementName();
    }
}