package by.a1qa.base;

import org.openqa.selenium.By;
import static by.a1qa.driver.BrowserFactory.*;
import static by.a1qa.utils.SimpleLogger.logger;

public abstract class BaseFrame {

    private final By locator;
    private final String name;
    
    public BaseFrame(By locator, String name) {
        this.locator = locator;
        this.name = name;
    }

    public void switchToFrame() {
        logger.info("Switching to frame: " + this.name);
        getDriver().switchTo().frame(getDriver().findElement(this.locator));
    }

    public void switchToDefaultContent() {
        logger.info("Switching to default content...");
        getDriver().switchTo().defaultContent();
    }
}