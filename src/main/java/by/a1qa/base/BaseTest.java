package by.a1qa.base;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import by.a1qa.utils.ConfigManager;

import static by.a1qa.driver.BrowserFactory.*;
import static by.a1qa.utils.SimpleLogger.logger;

public abstract class BaseTest {

    @BeforeSuite
    public void setLogger () {
        logger.info("TASK 3.2 test suite was started...\n");
    }

    @BeforeMethod
    public void invokeMainPage() {
        getDriver().get(ConfigManager.returnUrl());
    }

    @AfterMethod
    public void killDriver() {
        quitDriver();
        logger.info("\n");
    }
}