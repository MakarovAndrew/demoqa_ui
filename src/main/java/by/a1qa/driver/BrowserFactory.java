package by.a1qa.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import by.a1qa.models.Browser;
import by.a1qa.utils.ConfigManager;

public class BrowserFactory {

    private static WebDriver driver ;

    public static void closeDriver() {
        getDriver().close();
    }

    public static void quitDriver() {
        getDriver().quit();
        driver = null; //Не знаю как еще сделать :)
    }

    public static WebDriver getDriver() {
        if (driver == null) {
            setDriver();
        }
        return driver;
    }

    private static void setDriver() {

        Browser browser = ConfigManager.getBrowserConfig();

            switch (browser.getName().toUpperCase()) {

                case "FIREFOX":
                    {WebDriverManager.firefoxdriver().setup();
                    FirefoxOptions options = new FirefoxOptions();

                        if (!browser.getOptions().isEmpty()) {
                            for (String option : browser.getOptions()) {
                                options.addArguments(option);
                            }
                        }

                    driver = new FirefoxDriver(options);
                    break;}

                case "CHROME":
                    {WebDriverManager.chromedriver().setup();
                    ChromeOptions options = new ChromeOptions();

                        if (!browser.getOptions().isEmpty()) {
                            for (String option : browser.getOptions()) {
                                options.addArguments(option);
                            }
                        }

                    driver = new ChromeDriver(options);
                    break;}
                    
                default:
                    throw new RuntimeException("Incorrect browser name or the browser does not supported!");
            }
    }
}