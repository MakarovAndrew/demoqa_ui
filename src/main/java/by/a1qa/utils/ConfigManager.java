package by.a1qa.utils;

import by.a1qa.models.Browser;

public class ConfigManager {

    private static final String pathToBrowserSettings = "./src/main/resources/config/browser_settings.json";
    private static final String pathToTimeouts = "./src/main/resources/config/timeouts.json";
    private static final String pathToData = "./src/main/resources/data/test_data.json";
    private static final String pathToUrl = "./src/main/resources/config/main_url.json";

    public static Browser getBrowserConfig() {
        return JsonToObject.readBrowserObject(pathToBrowserSettings);
    }

    public static Integer getTimeout(String key) {
        return JsonToObject.getValueByKey(pathToTimeouts, key);
    }

    public static String getDataPath() {
        return pathToData;
    }

    public static String returnUrl() {
        return JsonToObject.getValueByKey(pathToUrl, "url");
    }
}