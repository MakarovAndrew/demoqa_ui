package by.a1qa.utils;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class JsonMapper {
    
    private static ObjectMapper mapper;

    private static void setMapper() {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static ObjectMapper getMapper() {
        if (mapper == null) {
            setMapper();
        }
        return mapper;
    }

    public static ObjectWriter getWriter(ObjectMapper mapper) {
        return mapper.writer(new DefaultPrettyPrinter());
    }
}