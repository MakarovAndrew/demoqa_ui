package by.a1qa.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class Randomizer {

    public static String returnRandomString(int lenght) {
        return RandomStringUtils.randomAlphabetic(lenght);
    } 
}