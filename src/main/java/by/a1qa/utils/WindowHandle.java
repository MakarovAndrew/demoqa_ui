package by.a1qa.utils;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import static by.a1qa.driver.BrowserFactory.*;
import static by.a1qa.utils.Waiters.*;
import static by.a1qa.utils.SimpleLogger.logger;

public class WindowHandle {

    public static void focusToNewTab (String originalHandle, int tabNumber) {

        if (waitUntilNewTabIsOpened(tabNumber, MEDIUM_TIMEOUT)) {
            for (String windowHandle : getDriver().getWindowHandles()) {
                if(!originalHandle.contentEquals(windowHandle)) {
                    getDriver().switchTo().window(windowHandle);
                    break;
                }
            } 
        }
    }

    public static void switchToWindow(String windowHandle) {
        getDriver().switchTo().window(windowHandle);
    }
    
    public static Alert switchToAlert() {
        return getDriver().switchTo().alert();
    }

    public static boolean isAlertPresent() {
        try {
            switchToAlert().equals(null);
            logger.info("Alert is present");
            return true;
        } catch (NoAlertPresentException e) {
            logger.info("Alert is not present!");
            return false;
        }
    }
}