package by.a1qa.utils;

import java.util.List;
import org.testng.annotations.DataProvider;

import by.a1qa.models.User;

public class DataProviderClass {
    
    private static final List<User> users = JsonToObject.readListFromJson(ConfigManager.getDataPath());
    private static Object[][] data = new Object[users.size()][1];

    @DataProvider (name = "fromFile")
    public static Object[][] dataProviderMethod() {

        for (int i = 0; i < data.length; i++) {
            data[i][0] = users.get(i);
        }

        return data;
    }
}