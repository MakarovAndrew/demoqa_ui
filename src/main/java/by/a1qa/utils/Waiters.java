package by.a1qa.utils;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.a1qa.base.BaseElement;

import static by.a1qa.driver.BrowserFactory.*;

public class Waiters {

    public static final int SHORT_TIMEOUT = ConfigManager.getTimeout("SHORT_TIMEOUT");
    public static final int MEDIUM_TIMEOUT = ConfigManager.getTimeout("MEDIUM_TIMEOUT");
    public static final int LONG_TIMEOUT = ConfigManager.getTimeout("LONG_TIMEOUT");

    public static WebElement waitUntilToBeClickable(WebElement element, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement waitUntilIsPresent(WebElement element, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.visibilityOf(element));
    }        

    public static boolean waitUntilNumberOfWindows(int tabsCount, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.numberOfWindowsToBe(duration));
    }     

    public static Alert waitUntilAlert(int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.alertIsPresent());
    }  
    
    public static boolean waitUntilStaleness(WebElement element, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.stalenessOf(element));
    }

    public static WebElement waitUntilVisibilityOfElementLocated(By locator, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static List<WebElement> waitUntilVisibilityOfAllElementsLocated(By locator, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public static WebElement waitUntilPresenceOfElementLocated(By locator, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static boolean waitUntilNewTabIsOpened(int tabsCount, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.numberOfWindowsToBe(tabsCount));
    }

    public static boolean waitUntilInvisiblityOfElementLocated(BaseElement element, int duration) {
        return (new WebDriverWait(getDriver(), Duration.ofSeconds(duration)))
            .until(ExpectedConditions.invisibilityOfElementLocated(element.getElementLocator()));
    }
}