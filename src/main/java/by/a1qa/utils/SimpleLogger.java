package by.a1qa.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class SimpleLogger {

    static {

        try (InputStream is = SimpleLogger.class.getClassLoader().getResourceAsStream("logging.properties")) 
        {
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger logger = Logger.getLogger(SimpleLogger.class.getName());
}