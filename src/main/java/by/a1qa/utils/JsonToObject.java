package by.a1qa.utils;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import by.a1qa.models.Browser;
import by.a1qa.models.User;

public class JsonToObject {

    public static <T> T getValueByKey(String path, String key) {

        try {
            return new HashMap<String,T>(JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<Map<String, T>>(){})).get(key);
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }

    public static List<User>  readListFromJson(String pathToJSON) {

        List<User> list = null;

        try {
            list = Arrays.asList(JsonMapper.getMapper().readValue(Paths.get(pathToJSON).toFile(), User[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Browser readBrowserObject(String pathToJSON) {

        Browser browser = null;

        try {
            browser = JsonMapper.getMapper().readValue(Paths.get(pathToJSON).toFile(), Browser.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return browser;
    }

    public static List<?> readObjectList (String path) {

        try {
            return Arrays.asList(JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<List<?>>(){}));
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }
    
    public static <T> T readObject (String path) {
        try {
            return JsonMapper.getMapper().readValue(Paths.get(path).toFile(), new TypeReference<T>(){});
        } catch (IOException e) {
            throw new RuntimeException("File not found by path: " + path);
        }
    }

}