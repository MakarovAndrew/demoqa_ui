package by.a1qa.models;

public class User {

    private String firstName;
    private String lastName;
    private String email;
    private String age;
    private String salary;
    private String departament;

    public User() {};

    public User(String firstName, String lastName, String email, String age, String salary, String departament) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.salary = salary;
        this.departament = departament;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }
    public String getAge() {
        return age;
    }
    public String getSalary() {
        return salary;
    }
    public String getDepartament() {
        return departament;
    }

    @Override
    public String toString () {
        
        return "User:\n     firstName \"" + this.firstName +
        "\"\n     lastName \"" + this.lastName +
        "\"\n     email \"" + this.email +
        "\"\n     age \"" + this.age +
        "\"\n     salary \"" + this.salary +
        "\"\n     departament \"" + this.departament + "\"";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        User otherUser = (User) obj;

        return firstName.equals(otherUser.firstName) &&
            lastName.equals(otherUser.lastName) &&
            email.equals(otherUser.email) &&
            age.equals(otherUser.age) &&
            salary.equals(otherUser.salary) &&
            departament.equals(otherUser.departament);
    }
}