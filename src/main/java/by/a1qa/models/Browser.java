package by.a1qa.models;

import java.util.List;

public class Browser {
       
        private String name;
        private List<String> options;

        public Browser(String name, List<String> options) {
            this.name = name;
            this.options = options;         
        }

        public Browser() {}

        public String getName() {
            return this.name;
        }

        public List<String> getOptions() {
            return this.options;
        }
}